var version = "1.0.2";

importScripts('node_modules/sw-toolbox/sw-toolbox.js');

var swCaches = {
    'static': 'static-' + version,
    'dynamic': 'dynamic-' + version
};

var filesToCache = [
    '/',
    "./index.html",
    "./android-icon-144x144.png",
    "./android-icon-192x192.png",
    "./android-icon-36x36.png",
    "./android-icon-48x48.png",
    "./android-icon-72x72.png",
    "./android-icon-96x96.png",
    "./apple-icon-114x114.png",
    "./apple-icon-120x120.png",
    "./apple-icon-144x144.png",
    "./apple-icon-152x152.png",
    "./apple-icon-180x180.png",
    "./apple-icon-57x57.png",
    "./apple-icon-60x60.png",
    "./apple-icon-72x72.png",
    "./apple-icon-76x76.png",
    "./apple-icon-precomposed.png",
    "./apple-icon.png",
    "./favicon-16x16.png",
    "./favicon-32x32.png",
    "./favicon-96x96.png",
    "./favicon.ico",
    "./index.html",
    "./ms-icon-144x144.png",
    "./ms-icon-150x150.png",
    "./ms-icon-310x310.png",
    "./ms-icon-70x70.png",
    "./images/bd/hilo.png",
    "./images/bd/hilo2.png",
    "./images/bd/logo.png",
    "./images/bd/nfums1.png",
    "./images/bd/nfums2.png",
    "./scripts/higherlower.js",
    "./scripts/nfums1.js",
    "./scripts/nfums2.js",
    "./scripts/nfums3.js",
    "./scripts/jquery-3.3.1.min.js",
    "./styles/bootstrap/bootstrap.css",
    "./node_modules/sw-toolbox/companion.js",
    "./node_modules/sw-toolbox/sw-toolbox.js",
	"./images/logo.jpg",
    "./scripts/bootstrap/bootstrap.bundle.js",
    "./scripts/bootstrap/bootstrap.min.js",
    "./styles/bootstrap/bootstrap-reboot.css",
    "./styles/bootstrap/bootstrap-grid.css"
];

/*
var imagesToCache = [
"1.jpg",
"1278849_630568593650812_1194609185_o_1_.jpg",
"180531_NFUM_Diversify_00450.jpg",
"180531_NFUM_Diversify_00762.jpg",
"2.jpg",
"3.jpg",
"33.jpg",
"39.jpg",
"4.jpg",
"41.jpg",
"5.JPG",
"512logo.png",
"6.jpg",
"7.jpg",
"8.jpg",
"gate.jpg",
"hilologo.png",
"ice-562684_1280.jpg",
"landscape17.jpg",
"nfulogo.png",
"nfums1.png",
"nfums2.png",
"nfums3.png",
"NFU_logo.svg",
"Pad.jpg",
"s3.png",
"snowwateringcan.jpg"
];*/

self.addEventListener('install', function (e) {
    console.log('[ServiceWorker] v%s Install', version);
    e.waitUntil(
        caches.open(swCaches.static).then(function (cache) {
            console.log('[ServiceWorker] Caching static resources to ' + swCaches.static);
            return cache.addAll(filesToCache);
        }).catch(console.error)
    );
});

toolbox.router.get('/scripts/*', toolbox.networkFirst, {
    cache: {
        name: swCaches.static,
        maxAgeSeconds: 60 // 1 min
        //maxAgeSeconds: 60 * 60 * 24 * 365 // 1 year
    }
});

toolbox.router.get('/fonts/*', toolbox.cacheFirst, {
    cache: {
        name: swCaches.static,
        maxAgeSeconds: 60 // 1 min
        //maxAgeSeconds: 60 * 60 * 24 * 365 // 1 year
    }
});

toolbox.router.get('/images/bd/*', toolbox.cacheFirst, {
    cache: {
        name: swCaches.static,
        maxAgeSeconds: 60 // 1 min
        //maxAgeSeconds: 60 * 60 * 24 * 365 // 1 year
    }
});

toolbox.router.get('/styles/*', toolbox.networkFirst, {
    cache: {
        name: swCaches.static,
        maxAgeSeconds: 60 // 1 min
        //maxAgeSeconds: 60 * 60 * 24 * 365 // 1 year
    }
});

toolbox.router.get('/*', function (request, values, options) {
    return toolbox.networkFirst(request, values, options)
        .catch(console.error) //ToDo serve offline content from cache here
}, {
        networkTimeoutSeconds: 5,
        cache: {
            name: swCaches.dynamic,
            maxEntries: 200
        }
    });



self.addEventListener('fetch', function (event) {
    console.log("[Service Worker] fetch request ");
    event.respondWith(
        caches.open(swCaches.dynamic).then(function (cache) {
            console.log("fetch request ");
            return cache.match(event.request).then(function (response) {
                return response || fetch(event.request).then(function (response) {
                    cache.put(event.request, response.clone());
                    return response;
                });
            });
        })
    );
});

