var setup;

var topics;

var questions;

var HigherLowerAPIURL = 'https://higherlowerapi.i-wareness.com';

var CompetitionAPIURL = 'http://localhost:5555';

var competitionStartDate = new Date("2018-10-11");

var competitionEndDate = new Date("2018-10-30");


var ImageDirectory = 'images/nfum/';

var gameID;

var startedNewTopic = false;

var currentTopic = 0;

var maxQuestions = 0;

var maxDifference = 0;

var currentQuestion = 0;

var questionSet;

var order;

var incorrectCount;

var progressunits = 0;

var incorrectAnswers;

var myhostname = "highlowbrewindolphin";

var leaderboardScores = [{
    name: "Nick", score: 30
}, {
    name: "Bob", score: 30
}, {
    name: "Frank", score: 30
}, {
    name: "Alf", score: 30
}, {
    name: "Cormorant", score: 30
}, {
    name: "Penguin", score: 30
}, {
    name: "Greg", score: 30
}, {
    name: "Seagull", score: 30
}, {
    name: "Pigeon", score: 29
}, {
    name: "Glorp", score: 29
}];


var mql = window.matchMedia("(orientation: portrait)");

$('#gameStatement').hide;
$('#gameStage').hide();
$('#scoreboard').hide();
$('#gameResults').hide();
$('#leaderboard').hide();

$(document).ready(function () {

    var now = new Date();

    if (now <= competitionEndDate && now >= competitionStartDate) {
        $('#challengePeriod').modal('show');
        $('.competitionPeriod').show();
    }

    window.scrollTo(0, 1);

    var removeTransitions = function () {
        $("#q1").addClass("notransitions");
        $("#q2").addClass("notransitions");
        $("#q3").addClass("notransitions");
    };

    var addTransitions = function () {
        $("#q1").removeClass("notransitions");
        $("#q2").removeClass("notransitions");
        $("#q3").removeClass("notransitions");
    };


    var addNewQuestionNoRepeat = function (topicID) {
        var newQuestion;

        for (i = 0; i < questions.length; i++) {
            newQuestion = questions[i];
            if (newQuestion.topicID === topicID && questionSet.length === 0) {
                questions.splice(i, 1);
                break;
            }
            else if (newQuestion.topicID === topicID &&
                questionSet.length > 0 && newQuestion !== questionSet[questionSet.length - 1] &&
                newQuestion.rating !== questionSet[questionSet.length - 1].rating) {
                questions.splice(i, 1);
                break;
            }

        }

        if (newQuestion === undefined || newQuestion === null)
            console.log("no question could be found");
        else
            questionSet.push(newQuestion);

        console.log("pushed new question in position  " + questionSet.length);

    };


    var addNewQuestionRepeat = function (topicID) {
        var tries = 100;
        var suitableQuestion = false;

        while (!suitableQuestion && tries > 0) {
            var newQuestion = questions[Math.floor(Math.random() * questions.length)];
            //console.log(newQuestion);
            if (questionSet.length === 0 && newQuestion.topicID === topicID) {
                questionSet.push(newQuestion);
                suitableQuestion = true;
                console.log("pushed new question in position 0");
            }
            else if (questionSet.length > 0 && newQuestion !== questionSet[questionSet.length - 1] &&
                newQuestion.rating !== questionSet[questionSet.length - 1].rating &&
                newQuestion.topicID === topicID && newQuestion.statement !== questionSet[questionSet.length - 1].statement) {
                questionSet.push(newQuestion);
                suitableQuestion = true;
                console.log("pushed new question in position " + questionSet.length);
            }
            tries--;
        }

    };

    var addNewQuestion = function (topicID) {
        if (setup.repeatQuestions === true)
            addNewQuestionRepeat(topicID);
        else
            addNewQuestionNoRepeat(topicID);
    };


    var hideShowAttributesFromDB = function (questionSetItem, qNo) {
        if (!questionSet[questionSetItem].showComparison)
            $('#q' + qNo + 'Comparison').addClass("invisible");
        else
            $('#q' + qNo + 'ComparisonStrapline').removeClass("invisible");

        if (!questionSet[questionSetItem].showRating) {
            $('#q' + qNo + 'Rating').addClass("invisible");
            $('#q' + qNo + 'has').removeClass("invisible");
            $('#q' + qNo + 'has').text(questionSet[questionSetItem].comparisonStrapline);
        }
        else {
            $('#q' + qNo + 'Rating').removeClass("invisible");
            $('#q' + qNo + 'has').addClass("invisible");
            $('#q' + qNo + 'has').text(questionSet[questionSetItem].comparisonStrapline);
        }

        if (!questionSet[questionSetItem].showComparisonStrapline)
            $('#answer' + qNo).addClass("invisible");
        else
            $('#answer' + qNo).removeClass("invisible");

    };

    var getNextQuestion = function () {

        currentQuestion++;

        console.log("currently on question " + currentQuestion + " of " + maxQuestions);


        var topicID = topics[currentTopic].topicID;



        topicID = topicID.toLowerCase();

        //if we are changing topic, we need to kill off current question set, and reset

        if (currentQuestion >= maxQuestions) {
            showScorePage();
        }
        else {
            if (currentQuestion !== maxQuestions - 1) {
                addNewQuestion(topicID);
                //we want to set order[2] to next question details#

                $('.' + order[2] + 'Question').text(questionSet[questionSet.length - 1].statement);
                $('.' + order[2] + 'Comparison').text(questionSet[questionSet.length - 1].comparison);
                $('.' + order[2] + 'Rating').text(questionSet[questionSet.length - 1].rating);
                $('.' + order[2] + 'Strapline').text(topics[currentTopic].topic + " than " + questionSet[questionSet.length - 1].statement);
                $('#' + order[2]).css("background-image", "url(" + ImageDirectory + questionSet[questionSet.length - 1].backgroundImageFilename + ")");

                hideShowAttributesFromDB(questionSet.length - 1, order[2].substr(1));


                if (questionSet[questionSet.length - 1].textColor !== null) {
                    $('.' + order[2] + 'Question').css("color", questionSet[questionSet.length - 1].textColor);
                    $('.' + order[2] + 'toanswer').css("color", questionSet[questionSet.length - 1].textColor);
                    $('.' + order[2] + 'Comparison').css("color", questionSet[questionSet.length - 1].textColor);
                    $('.' + order[2] + 'Rating').css("color", questionSet[questionSet.length - 1].textColor);
                }

                console.log("uses random image: " + questionSet[questionSet.length - 1].useRandomImage);
                console.log("background image link: " + questionSet[questionSet.length - 1].backgroundImageFilename);
                console.log("background image id: " + questionSet[questionSet.length - 1].backgroundImageID);
                console.log("background-color: " + questionSet[questionSet.length - 1].backgroundColor);



                var item = '#' + order[2];
                if (questionSet[questionSet.length - 1].backgroundColor !== null) {
                    $(item).css("background-color", bgcolors[currentQuestion-1]);
                }
                else if (questionSet[questionSet.length - 1].useRandomImage === true) {
                    setBackgroundImage(topics[currentTopic].topicID, true, item);
                }
                else if (questionSet[questionSet.length - 1].backgroundImageID !== null && questionSet[questionSet.length - 1].backgroundImageID !== "00000000-0000-0000-0000-000000000000") {
                    setBackgroundImage(questionSet[questionSet.length - 1].backgroundImageID, false, item);
                }
                else if (questionSet[questionSet.length - 1].backgroundImageFilename === null) {
                    $(item).css("background-image", "none");
                }
            }
        }

    };

    var bgcolors = ['#66C5EE', '#B2B2B2', '#D1D1D1', '#173245', '#74848F', '#009FE3', '#66C5EE', '#B2B2B2', '#D1D1D1', '#173245', '#74848F', '#009FE3', '#66C5EE', '#B2B2B2', '#D1D1D1', '#173245', '#74848F', '#009FE3', '#66C5EE', '#B2B2B2', '#D1D1D1'];

    var initialbgcolors = ['#173245', '#74848F', '#009FE3', '#66C5EE', '#B2B2B2', '#D1D1D1', '#173245', '#74848F', '#009FE3', '#66C5EE', '#B2B2B2', '#D1D1D1', '#173245', '#74848F', '#009FE3', '#66C5EE', '#B2B2B2', '#D1D1D1', '#173245', '#74848F', '#009FE3', '#66C5EE', '#B2B2B2', '#D1D1D1'];

    var moveToEnd = function () {
        removeTransitions();
        $("#" + order[0]).css({ 'z-index': 200 });

        $("#" + order[0]).css({ 'z-index': 100 });
        $("#" + order[1]).css({ 'z-index': 110 });
        $("#" + order[2]).css({ 'z-index': 120 });

        if (window.innerHeight < window.innerWidth)
            $("#" + order[0]).css({ left: '100%' });
        else
            $("#" + order[0]).css({ top: '100%' });


        $("#" + order[0]).removeClass("notransition");

        order[3] = order[0];
        order[0] = order[1];
        order[1] = order[2];
        order[2] = order[3];


        $(".higher").removeAttr("disabled");
        $(".lower").removeAttr("disabled");
        $('.testtransition').removeAttr("disabled");

        getNextQuestion();
    };

    var setModalWidowAnswers = function () {

        $('.QuestionQuestion').text(questionSet[questionSet.length - 1].statement);
        $('.QuestionComparison').text(questionSet[questionSet.length - 1].comparison);
        $('.QuestionRating').text(questionSet[questionSet.length - 1].rating);


        if (questionSet[questionSet.length - 1].showRating)
            $('.QuestionRating').removeClass("invisible");
        else
            $('.QuestionRating').addClass("invisible");

        $('.AnswerQuestion').text(questionSet[questionSet.length - 2].statement);
        $('.AnswerComparison').text(questionSet[questionSet.length - 2].comparison);
        $('.AnswerRating').text(questionSet[questionSet.length - 2].rating);


        if (questionSet[questionSet.length - 2].showRating)
            $('.AnswerQuestion').removeClass("invisible");
        else
            $('.AnswerRating').addClass("invisible");

        if (!questionSet[questionSet.length - 1].showRating) {
            if (questionSet[questionSet.length - 1].rating > questionSet[questionSet.length - 2].rating)
                $("#HLComparison").text("is risker than");
            else
                $("#HLComparison").text("is less risky than");
        }

    };


    var transitionQuestions = function () {

        setModalWidowAnswers();

        for (i = 0; i < order.length - 1; i++) {
            if (i === 2) {
                $("." + order[i] + "toanswer").fadeIn();
                $("." + order[i] + "toquestion").hide();
                $("#" + order[i]).addClass('activequestion');
            }
            else {
                $("." + order[i] + "toanswer").hide();
                $("." + order[i] + "toquestion").fadeIn();
                $("#" + order[i]).removeClass('activequestion');
            }
        }

        if (!mql.matches) {
            console.log("repositioning windows in portrait");
            $("#" + order[0]).css({ left: '-50%', top: '0' });
            $("#" + order[1]).css({ left: '0', top: '0' });
            $("#" + order[2]).css({ left: '50%', top: '0' });
            $("#" + order[0]).css({ left: '-50%', top: '0' });
            $("#" + order[1]).css({ left: '0', top: '0' });
            $("#" + order[2]).css({ left: '50%', top: '0' });

        }
        else {
            console.log("repositioning windows in landscape");
            $("#" + order[0]).css({ top: '-50%', left: '0' });
            $("#" + order[1]).css({ top: '0', left: '0' });
            $("#" + order[2]).css({ top: '50%', left: '0' });
        }

        $(".higher").attr("disabled", "disabled");
        $(".lower").attr("disabled", "disabled");
        $('.testtransition').attr("disabled", "disabled");
        window.setTimeout(moveToEnd, 500);

    };

    $('#gameStatement').show();
    $('#gameStage').hide();
    $('#scoreboard').hide();
    $('#gameResults').hide();
    $('#leaderboard').hide();


    var showScorePage = function () {
        $('#gameStatement').hide();
        $('#gameStage').hide();
        $('#scoreboard').hide();
        $('#gameResults').show();
        $('#leaderboard').hide();
    };


    var showLeaderboard = function (scores) {

        console.log("show the leaderboard");
        var leaderboardT = "<table class='table'>";
        leaderboardT += "<tr>";
        leaderboardT += "<th>Position</th>";
        leaderboardT += "<th>Name</th>";
        leaderboardT += "<th>Score</th>";
        leaderboardT += "</tr>";

        for (var i = 0; i < scores.length; i++) {
            leaderboardT += "<tr>";
            leaderboardT += "<td>" + (i + 1) + "</td>";
            leaderboardT += "<td>" + scores[i].name + "</td>";
            leaderboardT += "<td>" + scores[i].score + "</td>";
            leaderboardT += "</tr>";
        }

        leaderboardT += "</table>";

        $('#leaderboard-container').html(leaderboardT);

        $('#gameStatement').hide();
        $('#gameStage').hide();
        $('#scoreboard').hide();
        $('#gameResults').hide();
        $('#leaderboard').show();
    };


    var checkAnswer = function (higherOrLower) {

        var answer = -1;
        console.log("(questionSet[questionSet.length - 2].rating > questionSet[questionSet.length - 3].rating: " + questionSet[questionSet.length - 2].rating + " > " + questionSet[questionSet.length - 3].rating);
        if (questionSet[questionSet.length - 2].rating > questionSet[questionSet.length - 3].rating)
            answer = 1;

        if (currentQuestion === 14) {
            console.log("on final question")
            console.log("(questionSet[questionSet.length - 1].rating > questionSet[questionSet.length - 2].rating: " + questionSet[questionSet.length - 1].rating + " > " + questionSet[questionSet.length - 2].rating);
       
            if (questionSet[questionSet.length - 1].rating > questionSet[questionSet.length - 2].rating)
                answer = 1;
            else
                answer = -1;
        }

        if (higherOrLower === answer)
            $('#correctAnswer').modal('show');
        else
            $('#incorrectAnswer').modal('show');

    };

    var updateScore = function () {
        var currentScore = parseInt($('.score').first().text());
        $('.score').text(currentScore + 1);

    };


    //as the game gets further on, we want it to become increasingly hard
    //we will do this by decreasing the max question difference, and increasing the min question difference

    //var getPctComplete = function (maxQuestions, currentQuestion) {
    //    var pct = Math.floor(currentQuestion / maxQuestions * 100);
    //    console.log("game is " + pct + "% compelte");
    //    return pct;
    //};

    //var getMaxQuestionDifference = function (maxDifference, maxQuestions, currentQuestion) {

    //    return maxQuestions;
    //    //below code doesn't work when keeeping same questions
    //    /*
    //    if (getPctComplete(maxQuestions, currentQuestion) < 25) {
    //        return maxDifference;
    //    }
    //    if (getPctComplete(maxQuestions, currentQuestion) < 50) {
    //        return Math.floor(maxDifference * 0.8);
    //    }
    //    if (getPctComplete(maxQuestions, currentQuestion) < 75) {
    //        return Math.floor(maxDifference * 0.6);
    //    }
    //    if (getPctComplete(maxQuestions, currentQuestion) < 95) {
    //        return Math.floor(maxDifference * 0.4);
    //    }
    //    if (getPctComplete(maxQuestions, currentQuestion) <= 100) {
    //        return Math.floor(maxDifference * 0.2);
    //    }*/
    //};

    //var getMinQuestionDifference = function (maxDifference, maxQuestions, currentQuestion) {
    //    return 1;
    //    //below code doesn't work when keeeping same questions
    //    /*
    //        if (getPctComplete(maxQuestions, currentQuestion) < 25) {
    //            return Math.floor(maxDifference * 0.7);
    //        }
    //        if (getPctComplete(maxQuestions, currentQuestion) < 50) {
    //            return Math.floor(maxDifference * 0.6);
    //        }
    //        if (getPctComplete(maxQuestions, currentQuestion) < 75) {
    //            return Math.floor(maxDifference * 0.4);
    //        }
    //        if (getPctComplete(maxQuestions, currentQuestion) < 95) {
    //            return Math.floor(maxDifference * 0.2);
    //        }
    //        if (getPctComplete(maxQuestions, currentQuestion) <= 100) {
    //            return Math.floor(maxDifference * 0.1);
    //        }*/
    //};



    var getInitialQuestionSet = function (questions, maxDifference, maxQuestions, currentQuestion, topicID) {
        /*        var maxQuestionDifference = getMaxQuestionDifference(maxDifference, maxQuestions, currentQuestion);
                var minQuestionDifference = getMinQuestionDifference(maxDifference, maxQuestions, currentQuestion);
                */

        questionSet = new Array();
        addNewQuestion(topicID);
        addNewQuestion(topicID);
        addNewQuestion(topicID);
        return questionSet;
    };



    var positionQuestions = function () {
        if (!mql.matches) {
            console.log("repositioning windows in portrait");
            $("#" + order[0]).css({ left: '0', top: '0' });
            $("#" + order[1]).css({ left: '50%', top: '0' });
            $("#" + order[2]).css({ left: '100%', top: '0' });

        }
        else {
            console.log("repositioning windows in landscape");
            $("#" + order[0]).css({ top: '0', left: '0' });
            $("#" + order[1]).css({ top: '50%', left: '0' });
            $("#" + order[2]).css({ top: '100%', left: '0' });
        }
    };

    var image;

    var setBackgroundImage = function (id, random, item) {

        var backgroundImageURL = HigherLowerAPIURL + '/image/' + id + "/" + random;
        //ensure we won't get same image if using a random

        if (random && image !== undefined && image !== null)
            backgroundImageURL += "/" + image.imageID;

        fetch(backgroundImageURL).then((resp) => resp.json())
            .then(function (response) {
                console.log("successfully retreived image");
                image = response;
                if (image !== null)
                    $(item).css("background-image", "url(" + image.imageBase64 + ")");
            })
            .catch(function () {
                //TODO handle errors.
                console.log("failed to retreive image");
                image = null;
            });
    };

    var initialSetupQuestions = function (topicID) {

        console.log("initialise questions");
        getInitialQuestionSet(questions, maxDifference, maxQuestions, currentQuestion, topicID);


        for (i = 0; i < questionSet.length; i++) {
            $('.q' + (i + 1) + 'Question').text(questionSet[i].statement);
            $('.q' + (i + 1) + 'Comparison').text(questionSet[i].comparison);
            $('.q' + (i + 1) + 'Rating').text(questionSet[i].rating);
            $('.q' + (i + 1) + 'Strapline').text(topics[currentTopic].topic + " than " + questionSet[i].statement);
            $('#q' + (i + 1)).css("background-image", "url(" + ImageDirectory + questionSet[i].backgroundImageFilename + ")");


            hideShowAttributesFromDB(i, i + 1);


            if (questionSet[questionSet.length - 1].textColor !== null) {
                $('.q' + (i + 1) + 'Question').css("color", questionSet[questionSet.length - 1].textColor);
                $('.q' + (i + 1) + 'toanswer').css("color", questionSet[questionSet.length - 1].textColor);
                $('.q' + (i + 1) + 'Comparison').css("color", questionSet[questionSet.length - 1].textColor);
                $('.q' + (i + 1) + 'Rating').css("color", questionSet[questionSet.length - 1].textColor);
            }



            console.log("uses random image: " + questionSet[i].useRandomImage);
            console.log("background image link: " + questionSet[i].backgroundImageFilename);
            console.log("background image id: " + questionSet[i].backgroundImageID);
            console.log("background-color: " + questionSet[i].backgroundColor);

            var item = '#q' + (i + 1);

            if (questionSet[i].backgroundColor !== null) {
                $('#q' + (i + 1)).css("background-color", initialbgcolors[i]);
            }
            if (questionSet[i].useRandomImage === true) {
                setBackgroundImage(topics[currentTopic].topicID, true, item);
            }
            else if (questionSet[i].backgroundImageID !== null && questionSet[i].backgroundImageID !== "00000000-0000-0000-0000-000000000000") {
                setBackgroundImage(questionSet[i].backgroundImageID, false, item);
            }
            else if (questionSet[i].backgroundImageID.backgroundImageFilename === null) {
                $(item).css("background-image", "none");
            }
        }

        $('.q1toquestion').show();
        $('.q2toquestion').hide();
        $('.q3toquestion').hide();

        $('.q1toanswer').hide();
        $('.q2toanswer').show();
        $('.q3toanswer').show();


        $('#q2').addClass('activequestion');
        positionQuestions();
        setModalWidowAnswers();

        $('.QuestionQuestion').text(questionSet[questionSet.length - 2].statement);
        $('.QuestionComparison').text(questionSet[questionSet.length - 2].comparison);
        $('.QuestionRating').text(questionSet[questionSet.length - 2].rating);

        $('.AnswerQuestion').text(questionSet[questionSet.length - 3].statement);
        $('.AnswerComparison').text(questionSet[questionSet.length - 3].comparison);
        $('.AnswerRating').text(questionSet[questionSet.length - 3].rating);
        console.log("sucessessfully got question set");
    };


    $(".tryAgain").click(function () {
        initSetup();
    });

    var updateProgressBar = function () {
        //0 based qarray, 1 question behind
        $('#inner-progress').css({ "width": (progressunits * (currentQuestion + 1)) + "%" });
    };



    var moveOn = function () {
        topics[currentTopic].numberOfQuestions -= 1;
        console.log("number of questions in topic =" + topics[currentTopic].numberOfQuestions);



        if (currentQuestion + 1 == maxQuestions) {
            showScorePage();
        }
        else {
            if (topics[currentTopic].numberOfQuestions === 0) {
                console.log("started new topic");
                currentTopic += 1;
                startedNewTopic = true;
                if (topics[currentTopic] !== undefined) {
                    initialSetupQuestions(topics[currentTopic].topicID);
                    for (var i = 0; i < order.length; i++) {
                        order[i] = "q" + (i + 1);
                    }
                    positionQuestions();
                }
                else {
                    transitionQuestions();
                }

            }
            else
                transitionQuestions();
        }


        updateProgressBar();
    };

    $("#correctAnswer").on('hide.bs.modal', function () {
        moveOn();
    });

    $("#thankYouForEntering").on('hide.bs.modal', function () {
        initSetup();
    });

    $("#correctAnswer").on('show.bs.modal', function () {
        updateScore();
    });


    $("#incorrectAnswer").on('hide.bs.modal', function () {
        //todo track incorrect answers

        var incorrectAnswerSet = [];

        if (setup.continueOnIncorrect) {
            var q1 = questionSet[currentQuestion];
            var q2 = questionSet[currentQuestion + 1];
            incorrectAnswerSet.push(q1, q2);
            incorrectAnswers.push(incorrectAnswerSet);
            moveOn();
        }
        else
            showScorePage();

    });

    $("#submitScore").click(function () {
        showScorePage();
    });


    $('.lower').click(function () {
        addTransitions();
        checkAnswer(-1);
    });

    $('.higher').click(function () {
        addTransitions();
        checkAnswer(1);
    });

    $('#startGame').click(function () {


        
        $('#gameStatement').hide();
        $('#gameStage').show();
        if (setup.hasLeaderboard)
            $('#scoreboard').show();
        else
            $('#scoreboard').hide();
    });

    var validateEntry = function (entry) {
        //todo more robust js validation
        if (entry.name.length > 3 && entry.email.length > 6)
            return true;
        else
            return false;
    };

    var getEntry = function () {

        var email = $('#email').val();
        var name = $('#displayname').val();
        var score = parseInt($('.score').first().text());

        var entry = {
            "competitionId": gameID,
            "name": name,
            "email": email,
            "score": score,
            "metaData": incorrectAnswers
        };

        console.log("created entry");

        return entry;
    };

    $('#submitDetails').click(function () {
        //post details to competition api
        var entry = getEntry();

        console.log(entry);

        if (validateEntry(entry)) {

            //todo fix leaderboard
            fetch(CompetitionAPIURL + '/Enter/WithScore/', {
                method: "POST",
                mode: "no-cors",
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify(entry)

            })
                .then(function (response) {
                    return response.json();
                })
                .then(function () {
                    console.log("submitted score");
                    //get leaderboard
                })
                .then(function (response) {
                    //todo: complete leaderboard functionality
                    console.log("sucessfully retreived leaderboard");
                    if (setup.hasLeaderboard)
                        showLeaderboard(response);
                })
                .catch(function () {
                    console.log("error submitting leaderboard");
                });

            if (setup.hasCompetition) {

                console.log("user entered competition");
                //todo enter competition
                $('#thankYouForEntering').modal('show');
            }
        }
    });

    $('#tsAndCs').click(function () {
        $('#tsAndCsModal').modal('show');
    });


    Array.prototype.shuffle = function () {

        var sorted = false;
        this.sort(function (a, b) {
            return a.topic - b.topic;
        });
        while (sorted === false) {
            var n = this.length;
            while (n--) {
                var i = Math.floor(n * Math.random());
                var tmp = this[i];
                this[i] = this[n];
                this[n] = tmp;
            }
            for (var i = 1; i < this.length; i++) {
                if (this[i].rating == this[i - 1].rating) {
                    sorted = false;
                    break;
                }
                sorted = true;
            }
        }

        return this;
    };

    Array.prototype.shuffle2 = function () {
        var n = this.length;
        while (n--) {
            var i = Math.floor(n * Math.random());
            var tmp = this[i];
            this[i] = this[n];
            this[n] = tmp;
        }



        return this;
    };



    var initQuestions = function (gameID, startUp) {
        fetch(HigherLowerAPIURL + '/questions/' + gameID)
            .then((resp) => resp.json())
            .then(function (response) {
                console.log("sucessfully retreived questions");
                questions = response;
                questions = questions.shuffle();
                if (startUp)
                    initialSetupQuestions(topics[0].topicID);
            });

    };

    var initTopics = function (gameID) {
        console.log("initTopics");

        fetch(HigherLowerAPIURL + '/game/topicsetup/' + gameID)
            .then((resp) => resp.json())
            .then(function (response) {
                topics = response;
                initQuestions(setup.gameID, true);
            });


    };



    var initSetup = function () {

        console.log("init");


        $('#bgoverlay').css("opacity", "1");
        $('#gameStatement').show();
        $('#gameStage').hide();
        $('#scoreboard').hide();
        $('#gameResults').hide();
        $('#leaderboard').hide();


        var gameUrl = HigherLowerAPIURL + '/game/' + myhostname;

        fetch(gameUrl).then((resp) => resp.json())
            .then(function (response) {
                setup = response;
                //we need to add the ending date for the competition here to override the hascompetition tag

                if (competitionEndDate < new Date()) {
                    setup.hasCompetition = false;
                }





            }).then(function () {
                order = ["q1", "q2", "q3", ""];
                maxQuestions = setup.maxQuestions;
                maxDifference = setup.maxDifference;
                hasLeaderboard = setup.hasLeaderboard;


                $('#gamestatement').text(setup.gameStatement);
                $('.maxQuestions').text(maxQuestions);
                currentQuestion = 0;
                incorrectCount = 0;
                incorrectAnswers = [];
                currentTopic = 0;
                $('.score').text("0");
                gameID = setup.gameID;
                initTopics(setup.gameID);

                progressunits = 100 / maxQuestions;

                $('#inner-progress').css({ "width": 0 + "%" });

                $(".hasleaderboard").hide();

                if (hasLeaderboard) {
                    $(".hasleaderboard").show();
                    $('#submitDetails').text("Submit Score");
                }
                console.log("has competition:" + setup.hasCompetition);
                if (!setup.hasCompetition) {
                    console.log("no competition so hide ts and cs");
                    $('#tsAndCs').hide();
                    $(".hascompetition").hide();
                }

                if (!hasLeaderboard && !setup.hasCompetition) {
                    //hide the buttons and email etc.
                    $("#submitDetails").hide();
                    $(".form-group").hide();
                }

                if (setup.continueOnIncorrect)
                    $(".endOnIncorrect").hide();


                $('#tsAndCsContent').html(setup.tsAndCs);



            })
            .catch(function () {
                //TODO handle errors.
            });


    };

    initSetup();


    //listener used to detect orientation change
    mql.addListener(function (m) {
        positionQuestions();
    });

});

