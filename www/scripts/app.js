(function() {
  'use strict';

  if ('serviceWorker' in navigator) {
    navigator.serviceWorker
             .register('../service-worker.js')
             .then(data => console.log('Service Worker Registered at ', new Date().toLocaleTimeString()),
               err => console.error('Failed to register Service Worker at ', new Date().toLocaleTimeString()));
             
  }
})();
